@echo off
Echo Install NodeJS and Git
REM NodeJS
winget install -e --id OpenJS.NodeJS --accept-package-agreements
if %ERRORLEVEL% EQU 0 Echo NodeJS installed successfully.
REM Git
winget install -e --id Git.Git --accept-package-agreements
if %ERRORLEVEL% EQU 0 Echo Git installed successfully.   %ERRORLEVEL%
