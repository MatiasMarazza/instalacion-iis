DISM /online /enable-feature /featurename:IIS-WebServerRole  /featurename:IIS-WebServer  /featurename:IIS-CommonHttpFeatures  /featurename:IIS-HttpErrors  /featurename:IIS-ApplicationDevelopment  /featurename:IIS-Security  /featurename:IIS-URLAuthorization  /featurename:IIS-RequestFiltering  /featurename:IIS-HealthAndDiagnostics  /featurename:IIS-HttpLogging  /featurename:IIS-LoggingLibraries  /featurename:IIS-RequestMonitor  /featurename:IIS-Performance  /featurename:IIS-WebServerManagementTools  /featurename:IIS-ISAPIExtensions  /featurename:IIS-ISAPIFilter  /featurename:IIS-StaticContent  /featurename:IIS-CGI  /featurename:IIS-CustomLogging  /featurename:IIS-BasicAuthentication  /featurename:IIS-HttpCompressionStatic  /featurename:IIS-ManagementConsole  /featurename:IIS-DigestAuthentication

@echo RESULTADO ESPERADO
@echo *** Enabling feature(s)
@echo *** [==========================100.0%==========================]
@echo *** The operation completed successfully.
pause
