## Cómo utilizar este repo?
    Seguir la línea de pasos en la carpeta CMD
        Hay algunos archivos .md en la carpeta CMD, leerlos, explican el paso a seguir para ese proceso ya que no se ha automatizado todavía por cmd.
## IMPORTANTE     
La lectura de los archivos .md en esta carpeta es crucial para poder realizar la instalación, el usuario ha de saber que esta haciendo para poder realizar troubleshooting en caso de ser necesario, cada ambiente es distinto.

## ESTRUCTURA DE CARPETAS
## Carpeta Archivos
Posee instaladores y archivos necesarios para la correcta instalación y setup del sistema.
    Se utilizará solo cuando referenciada por los archivos en la carpeta CMD

## Carpeta CMD
Contiene los comandos de instalación junto con explicaciones en pasos, luego de leer los archivos .md ubicados en este nivel, esta carpeta debería ser su siguiente paso siguiendo los scripts e instrucciones que se ubican ahí.
## Carpeta sync
Se detalla en el archivo Scripts de syncro.md su utilización
    Se utilizará una vez instalado el sistema para actualizar precios, video y todo lo necesario para el correcto funcionamiento del sistema.
        En caso de que el Task manager falle, siempre se pueden ejecutar los comandos manualmente pero tener en cuenta que el tiempo total de scripts puede elevarse a 20-30 minutos fácilmente.
