## Explicación del paso a paso para debugging.


0- Se instala la herramienta winget para poder utilizarlo en pasos posteriores en la instalación de varios programas (Node y Git).
1- Se habilita IIS en el sistema operativo.
2-  Se utiliza winget para instalar Node y Git
3- Se descarga la version Non thread safe de PHP 8.1 desde https://windows.php.net/downloads/releases/php-8.1.13-nts-Win32-vs16-x64.zip  al disco C:/
4- Se extrae el php descargado a C:\php\
5- Se configura IIS y PHP
    El error más probable en este caso, es que la línea 6 del archivo
        set phpdir=C:\php\
    Esté mal ubicado el directorio de PHP o no se haya descomprimido correctamente, checkear que exista la ruta con los archivos de PHP que se han descargado y descomprimido en los pasos 3 y 4 respectivamente.
6-Se descarga el módulo Urlrewrite para IIS desde https://download.microsoft.com/download/1/2/8/128E2E22-C1B9-44A4-BE2A-5859ED1D4592/rewrite_amd64_es-ES.msi 
7-Se ejecuta el archivo descargado en el paso 6
    Un posible error en este paso, es que el archivo haya sido corrompido en la descarga, en caso de fallar por favor utilizar el link brindado.
8-Se ejecuta el instalador del módulo PHP Manager ubicado en la carpeta archivos
    En caso de falla por corrupción, puede ser descargado desde este link https://www.iis.net/downloads/community/2018/05/php-manager-150-for-iis-10 en la opción "Download this extension".
9-Se descarga Composer desde https://getcomposer.org/Composer-Setup.exe . Debe quedar el archivo Composer-Setup.exe en el disco C:/
10-Se ejecuta el archivo composer para proceder con su instalación, se debe aceptar sus requerimientos
        Seleccionar en la pestaña instalar para todos los usuarios
        Tildar el directorio PHP (Y checkear que se encuentre apuntando correctamente al PHP instalado en el punto 4)
        Y las siguientes pestañas, todo "next"
        Apretar finish
12-Se clona el repositorio https://gitlab.com/smartway1/carteleria-digital-2022/hoyts-cinemark-2022
    Se debe estar logeado con una cuenta en Gitlab para poder acceder al repositorio, caso contrario no podrá clonar el proyecto.
13-Se copia el repositorio desde C:\hoyts-cinemark-2022\  a C:\inetpub\wwwroot\hoyts-cinemark-2022
14- Se copia el archivo web.config ubicado en la carpeta archivos de este proyecto hacia la carpeta public del proyecto hoyts-cinemark-2022 en IIS ubicada en C:\inetpub\wwwroot\hoyts-cinemark-2022\backend\api\public\
15-Se configura el FastCGI
    Se han documentado fallas esporádicas en este paso, se ha agregado un paso de reduncia en pasos siguientes.
16-Se copia el archivo php.ini  ubicado en la carpeta archivos de este proyecto hacia la carpeta public del proyecto hoyts-cinemark-2022 en IIS ubicada en  C:\inetpub\wwwroot\hoyts-cinemark-2022\backend\api\public\
17-Se le otorgan permisos al IIS en la carpeta C:\inetpub\wwwroot\hoyts-cinemark-2022 para que pueda escribir y leer.
18-Se crea el sitio en IIS
    Ir hasta IIS, grupo de aplicaciones y click derecho en "backendito"
        Edit bindings
        La dirección IP debería ser 127.0.0.1
        Y el puerto 82
19-Se utiliza "composer install" en C:\inetpub\wwwroot\hoyts-cinemark-2022\backend\api\
20- Se instala mysql
    Se adjunta blog con paso a paso e imágenes https://www.sqlshack.com/how-to-install-mysql-database-server-8-0-19-on-windows-10/
20.1-Se crea la base de datos
    Se va a C:\Program Files\MySQL\MySQL Server 8.0\bin 
        Se puede utilizar cd C:\Program Files\MySQL\MySQL Server 8.0\bin
    Se entra a la terminal mysql con 
         mysql -u root -p
    Se pedirá la contraseña que designó en la instalación en el paso 20
    Escriba 
        create database smw_api
    para crear la base de datos
    Ya puede cerrar el terminal 
20.2 Se coloca la contraseña de la base de datos creada en el proyecto
    Si el sistema no deja modificar el archivo, crear una copia en el escritorio y pegarla en la ubicación del archivo en su lugar. Windows no dejará muchas veces modificar el archivo original, pero si reemplazarlo.
20.3. En este paso se abre IISManager y se establece la ruta del archivo php-cgi.exe (Esta ubicado en C:/php) para la utilizacion de PHP con IIS, aunque redundante con el paso 5, se ha establecido como necesario.
20.4-Se habilita la lectura de directorios para IIS, paso redundante con el paso 17 pero establecido necesario.
21-Se utiliza el comando" php artisan migrate --seed " en la carpeta C:\inetpub\wwwroot\hoyts-cinemark-2022\backend\api para llenar los datos en la tabla de datos
22-Se va al archivo host (ubicado en  c:\Windows\System32\Drivers\etc\hosts ) para agregar  api.smartsignane.smw y poder utilizar esa direccion en la barra de direcciones del navegador en lugar de localhost o 127.0.0.1
23-Se instalan las dependencias del front con " npm install --legacy-peer-deps" y se construye la versión de producción con "npm run build" en la carpeta C:\inetpub\wwwroot\hoyts-cinemark-2022\frontend\board
24-Se instalan las dependencias del front con " npm install"
25-Se inicia el servidor del frontend usando "npm start" en C:\inetpub\wwwroot\hoyts-cinemark-2022\frontend\board
26-Se inicia el servidor del servidor HLS usando "npm run dev" en C:\inetpub\wwwroot\hoyts-cinemark-2022\stream\hls
27- Se debe instalar FFMPEG
    Se adjunta un blog con el paso a paso e imágenes https://phoenixnap.com/kb/ffmpeg-windows
28- Se utiliza el comando php artisan csv:import en la carpeta C:\inetpub\wwwroot\hoyts-cinemark-2022\backend\api para sincronizar los CSV