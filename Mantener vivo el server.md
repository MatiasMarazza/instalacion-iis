## Servidor HLS y frontend
Por el momento, el servidor HLS y frontend deben manterse vivos en terminales
En la carperta ´´CMD´´ pasos 25 y 26 se levantan los servidores

## Frontend se levanta en localhost:3000
cd C:\inetpub\wwwroot\hoyts-cinemark-2022\frontend\board
npm start

## HLS se levanta en localhost:3200
cd C:\inetpub\wwwroot\hoyts-cinemark-2022\stream\hls
npm run dev